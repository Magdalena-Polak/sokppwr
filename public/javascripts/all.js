function addUser(){
    window.location.href = '/create_app_1';
}

function joinApplication(){
    window.location.href = '/join_application';
}

function joinApplicationId(id){
  popupS.window({
    mode: 'confirm',
    content : '<b>Czy na pewno chcesz kontynuować?</b>',
    labelOk:     'Dalej',
    labelCancel: 'Zamknij',
    onSubmit: function(){
      window.location.href = '/join_application/'+id;
    }, // gets called when submitted. val as an paramater for prompts
    onClose: function(){}      // gets called when popup is closed
  });
}

function goToHomePage(){
    window.location.href = '/';
}

function goBack(){
    history.go(-1);
}

function createNewApplication(){
    window.location.href = '/create_app_1';
}
